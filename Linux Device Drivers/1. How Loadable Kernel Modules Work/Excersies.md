1. Can an Ethernet driver be a loadable kernel module
	Yes if it is not needed as part of the boot process. Ethernet drivers as LKM are common. An Ethernet driver needed for an NFS root file system (common in embedded systems) *cannot* be a loadable kernel module and must be statically linked.

2. What are the steps in being able to use a *loadable* kernel module after editing one of its source files?
	1. Make the `.ko` file (`make installed`)
	2. If module is already loaded, remove it with `rmmode` or `modprobe -r`
	3. Install if using standard directory (`insmod` ) or skip if using custom directory (`make modules_install` loading with `modprobe`).

3. What are the steps in being able to use a *loadable* kernel module after editing one of its source files?
	1. No need for `make modules`
	2. Must rebuild a new kernel 
	3. Install new kernel
	4. Reboot system

4. How many modules are currently dynamically loaded in the kernel?
	Use the command `lsmod | wc -l` to count the number of lines (i.e., modules) that are currently loaded

5. How do you find the parameters for the `bluetooth` module?
	`modprobe -p bluetooth`

6. Where do you find the values of the parameters for the `bluetooth` module?
	`cat /sys/modules/bluetooth/parameters/*`