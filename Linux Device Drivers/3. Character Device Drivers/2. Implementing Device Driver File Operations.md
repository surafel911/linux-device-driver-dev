# File Operations

For character device files, file operations are executed using the [`file_operations`](https://tldp.org/LDP/lkmpg/2.4/html/c577.htm) struct. Its defined in `linux/fs.h`, and holds function pointers for callbacks defined by the driver that perform various operations on the device. They are equivalent to system calls that a process can use.

For example, when a process opens a device file and executes a `read` function on that file descriptor, it will call the `read` callback within the `file_operations` struct.

## open
```c
int (*open) (struct inode *, struct file *);
```

A device driver can implement the `open` function in the `file_operations` struct. 

The generic `open` function in the kernel is called before the `open` function defined by the driver. It may be sufficient to just use the default implementation.

The device driver can use the `inode` to get the minor number for the driver. The second parameter is the file object that was created for the process.

## read
```c
 ssize_t (*read) (struct file *, char *, size_t, loff_t *);
```

Put up to `size_t` bytes into the buffer, update the position in the file, and return the number of bytes read. It's okay to read less bytes than requested.

Return 0 when the end of the file is reached, or `-errno` on error. However the application will simply see a -1 since that is what the user space library returns.

## write
```c
ssize_t (*write) (struct file *, const char *, size_t, loff_t *);
```

## release
Release is called by the kernel when the last `close` function is called on the device file. It is the appropriate time to free resources. Forked processes cause copies of the file table entries: one `open()` but multiple `close()` may be called. Therefore it's unreliable to track the number of processes that have opened this file with the `open()` function, and you should rely on `release()` to free up resources.

# File Table and File Objects

Each process has an instance of the `task_struct`. Inside the `task_struct` is a pointer to `files_struct` which contains a pointer to the file table. The file table (i.e, `fdtable`) an object that stores a list of all open files for a process. Objects such as `stdout`, `stdin`,  and `stderr` are all subscripts within the file table with indices 0, 1, 2 respectively.

Inside of the `fdtable` object is a dynamic array of `file` objects. When a file is opened an instance of the `file` struct  is allocated which contains information about the state of the file, such as the file descriptor, position (e.g., file cursor),  flags (e.g., non-blocking read/write), etc.

Within the `file` struct is the instance of the `file_operations` struct that permits operations for the driver.

Since file objects are unique to each process, there can be multiple file objects if multiple processes have the same file opened. Likewise, when a child process is created, the child process inherits the file table from the parent process.

![[file-table-and-objects-graph.png]]


It's possible to observe these structures with the `cscope` program.