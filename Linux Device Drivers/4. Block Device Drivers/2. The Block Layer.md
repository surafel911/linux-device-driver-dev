# Role of the Block Layer

The block layer is responsible for managing the request queue - it can add, merge, and schedule requests so that access to the disk is faster, optimized, and often safer. 

It can also cache blocks, buffers, and requests which is particularly useful for non-block oriented file systems such as NFS. Knows information about the disk, such as the number of partitions and some metadata.

The key data structure for the block layer is `struct gendisk`.  `gendisk`  stores information about a disk. 

## Queuing Requests

The important part about the interaction the block layer and the block device driver the queuing of requests. Unlike character device drivers where you interact with driver by calling read and write, block device drivers work by making requests.

The Linux kernel has multiple algorithms for different request ordering and queuing disciplines that are configured by kernel configuration options and/or disk options.

You can see what schedulers are available by running `cat /sys/block/DEV/queue/scheduler`. Change the scheduler you want to use by writing the name into the scheduler file `echo $NAME /sys/block/DEV/queue/scheduler`

![[block-device-scheduler.png]]
## Caching 

An important part of the block layer is the caching of blocks. When you write to a file, a request is made for that write, but the write itself only copies the data into a memory cache. Often times for single / small requests, the block driver is not aware of the write.

Eventually the block layer will reorder or group the write requests and the memory will be written to disk by sending the requests to the block driver.

Likewise, `read` calls are read from the cache in memory first if the block is there. The kernel can read ahead so that blocks are already cached before a process asks for them.

The block layer provides a buffering mechanism for the block driver, unless direct I/O is enabled and the block layer is bypassed entirely.

You can see how much memory is being used as a buffer / cache for the block layer by running the `free -m` command:
![[block-layer-mem-consumption.png]]
