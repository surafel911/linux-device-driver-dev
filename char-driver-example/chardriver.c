// #include <string.h>
// #include <stdint.h>

#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/init.h>		/* Needed for the macros */
#include <linux/cdev.h>

#define DEFAULT_MAJOR_NUM 	197
#define DEFAULT_MINOR_NUM 	0
#define DEFAULT_DEBUG		0
#define DEFAULT_NAME		"chardriver"

static unsigned int major = DEFAULT_MAJOR_NUM;
static unsigned int minor = DEFAULT_MAJOR_NUM;
static unsigned int debug = DEFAULT_DEBUG;
static char* name =         DEFAULT_NAME;

/* Allow insmod to change these parameters. Include documentation. */
module_param(major, int, S_IRWXU);
MODULE_PARM_DESC(major,	"Used to override driver's major number");

module_param(minor, int, S_IRWXU);
MODULE_PARM_DESC(debug,	"Used to override driver's minor number");

module_param(debug, int, S_IRWXU);
MODULE_PARM_DESC(debug,	"Set to 1 for debugging output, 0 for no output");

module_param(name, charp, S_IRWXU);
MODULE_PARM_DESC(name, "The name of the character driver");

MODULE_AUTHOR("surafel911@gmail.com");
MODULE_DESCRIPTION("This is a very simple character driver");

/* Function Prototypes */
ssize_t chardriver_read(struct file *, char *, size_t, loff_t *);
ssize_t chardriver_write(struct file *, const char *, size_t, loff_t *);

/*
 * This must be made static since it is use after the current
 * function returns. It holds the methods used by the driver.
 */
static struct file_operations chardriver_fops = {
    .read = chardriver_read,
    .write = chardriver_write,
    .owner = THIS_MODULE,
};

#define BUFFER_SIZE	1024
static char buffer[BUFFER_SIZE];
static unsigned int buffer_length = 0;

/* The dev_t type is really just a uint32_t that contains the major and minor
 * number that uniquely identifies this particular instance of the device file.
 * Remember, it's possible for multiple device files to use the same device
 * driver.
 */
static dev_t chardriver_dev = 0;

/* This is a kernel struct that contains important info for character device
 * drivers. This is required.
 */
static struct cdev* chardriver_cdev = NULL;

/* Specify the number of minor numbers allowed. Recall that minor numbers are
 * separate instances of the same driver for multiple devices.
 */
#define NUM_MINORS	5

int chardriver_init_module(void) {
	int result = 0;

    /* Load character driver */
    chardriver_dev = MKDEV(major, 0);
    chardriver_cdev = cdev_alloc();
    result = register_chrdev_region(chardriver_dev, NUM_MINORS, name);

    /* Fail to load if register_chardriver fails */
    if (result < 0) {
        return result;
	}

    /* If major = 0, it if auto-allocated */
    if (!major) {
        major = result;
	}

    if (debug) {
        printk("chardriver registered with major = %d\n",major);
	}

    chardriver_cdev->ops = &chardriver_fops;
    chardriver_cdev->owner = THIS_MODULE;
	
    result = cdev_add(chardriver_cdev, chardriver_dev, NUM_MINORS);

	memset((void*)buffer, '\0', BUFFER_SIZE);

	return result;
}

void chardriver_cleanup_module(void) {
    unregister_chrdev_region(chardriver_dev, NUM_MINORS);
    cdev_del(chardriver_cdev);

	if (debug) {
		printk("The module is now unloaded\n");
	}
}

module_init(chardriver_init_module);
module_exit(chardriver_cleanup_module);
MODULE_LICENSE("GPL");

ssize_t chardriver_read(struct file* fp, char* buff, size_t nbytes, loff_t* ppos) {
    int available = 0, failed = 0, transferred = 0;

    if (debug)
        printk("READ: %d bytes\n",(int)nbytes);

    if (*ppos >= buffer_length || !nbytes)
        return 0;

    available = buffer_length - *ppos;
    if (nbytes > available)
        nbytes = available;

    failed = copy_to_user(buff, buffer + *ppos, nbytes);
    transferred = nbytes - failed;

    if (!transferred) {
        return -EFAULT;
	}

    *ppos += transferred;

    return transferred;
}

ssize_t chardriver_write(struct file* fp, const char* buff, size_t nbytes, loff_t* ppos) {
    int failed = 0, transferred = 0;

    if (debug)
        printk("WRITE: %d bytes\n",(int)nbytes);

    if (!nbytes)
        return 0;

    if (nbytes >=BUFFER_SIZE)
        nbytes = BUFFER_SIZE-1;

    failed = copy_from_user(buffer, buff, nbytes);
    transferred = nbytes - failed;

    if (!transferred) {
        return -EFAULT;
	}

    buffer[transferred] = '\0';

    printk("%s\n", buffer);

    buffer_length = transferred;
    return transferred;
}