#include <linux/module.h>

static int errval = 0;

module_param(errval, int, S_IRWXU);
MODULE_PARM_DESC(errval, "Value returned from init function");

void module_run(void) {
	printk("The module is now loaded\n");

	float f = 1.1f;
	int n = 99, m = 777;

	printk(KERN_EMERG "Testing emergency level");
	printk(KERN_ERR "Testing error level");
	printk(KERN_NOTICE "Testing notice level");
	printk(KERN_DEBUG "Testing debug level");

	printk("STARTING FLOATS\n");
	printk("using g: m=%d f=%g n=%d\n", m,f,n);
	printk("using f: m=%d f=%f n=%d\n", m,f,n);
	printk("using e: m=%d f=%e n=%d\n", m,f,n);
	printk("DONE\n");
}

int dev_init_module(void) {
	module_run();

	return errval;
}

void dev_exit_module(void) {
	printk("The module is now unloaded\n");
}

module_init(dev_init_module);
module_exit(dev_exit_module);
MODULE_LICENSE("GPL");
