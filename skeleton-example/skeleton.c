#include <linux/module.h>

int dev_init_module(void) {
	printk("The module is now loaded\n");
	return 0;
}

void dev_exit_module(void) {
	printk("The module is now unloaded\n");
}

module_init(dev_init_module);
module_exit(dev_exit_module);

MODULE_LICENSE("GPL");